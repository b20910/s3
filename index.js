/* 
    Mini-Activity

    Create an expressjs api running at port 4000.

    Make sure to add a message that the server is running.
*/

const express = require("express");
const app = express();
const PORT = 4000;

app.use(express.json());

let users = [
    {
        name: "John",
        age: 18,
        username: "johnsmith99"
    },
    {
        name: "Johnson",
        age: 21,
        username: "johnson1991"
    },
    {
        name: "Smith",
        age: 19,
        username: "smithMike12"
    }  
];

app.get('/users', (req,res) => {

    return res.send(users);

})

/* 
    Mini-Activity

    1. in Index.js:
        -initialize an array of objects for products. Each object should have the following fields:
            -name
            -price
            -isActive
        -Create a get method with the "/products" endpoint that returns the products array.
        -Create a new test suite with 3 test cases
            -test if the /products route is running. Assert that the res object should not be undefined.
            -test if the /products route is returning an array. Assert that the res.body is an array.
            -test that the first item in the array is an object.
*/

let products = [
    {
        name: "product1",
        price: "5000",
        isActive: true
    },
    {
        name: "product2",
        price: "2000",
        isActive: true
    },
    {
        name: "product3",
        price: "3000",
        isActive: true
    }
];

app.get('/products',(req,res) => {
    return res.send(products);
})

app.post('/users',(req,res) => {

    // add a simple if statement that if the request body does not have a name property we will send an error message along with a 400 http status code
    // .hasOwnProperty() returns a boolean if the property name passed exists or does not exist in the given object.
    if(!req.body.hasOwnProperty("name")){
        return res.status(400).send({
            error: "Bad Request - missing required parameter NAME"
        })
    }

    if(!req.body.hasOwnProperty("age")){
        return res.status(400).send({
            error: "Bad Request - missing required parameter AGE"
        })
    }

    if(!req.body.hasOwnProperty("username")){
        return res.status(400).send({
            error: "Bad Request - missing required parameter USERNAME"
        })
    }
})

app.listen(PORT,() => console.log(`Server is running at localhost:${PORT}`));